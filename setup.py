#!/usr/bin/env python3
"""Setup script for packaging checkin."""

import json
from pathlib import Path
from setuptools import setup, find_packages


def read(*parts):
    """Read the content of a file."""
    script_path = Path(__file__).parent
    with script_path.joinpath(*parts).open() as f:
        return f.read()


def find_key(pck_name: str = "rechunk_data", key: str = "__version__"):
    vers_file = Path(__file__).parent / "src" / pck_name / "__init__.py"
    with vers_file.open() as f:
        for line in f.readlines():
            if key in line:
                return json.loads(line.split("=")[-1].strip())
    raise ValueError(f"{key} not found in {pck_name}")


setup(
    name="rechunk_data",
    version=find_key("rechunk_data"),
    author="Martin Bergemann",
    author_email="bergemann@dkrz.de",
    maintainer="Martin Bergemann",
    url="https://gitlab.dkrz.de/k204230/install-kernelspec",
    description="Rechunk netCDF4 to optimal chunksize.",
    long_description=read("README.md"),
    license="WTFPL",
    packages=find_packages("src"),
    package_dir={"": "src"},
    entry_points={
        "console_scripts": [f"{find_key(key='PROGRAM_NAME')} = rechunk_data:cli"]
    },
    install_requires=["argparse", "dask", "xarray", "h5netcdf", "netCDF4", "typing_extensions"],
    extras_require={
        "test": [
            "black",
            "mock",
            "mypy",
            "nbformat",
            "pytest",
            "pylint",
            "pytest-html",
            "pytest-env",
            "pytest-cov",
            "testpath",
            "types-mock",
        ],
    },
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
    ],
)
