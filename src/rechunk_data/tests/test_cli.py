"""Unit tests for the cli."""

from pathlib import Path
from tempfile import TemporaryDirectory

import pytest
from rechunk_data import cli


def test_command_line_interface(data_dir: Path) -> None:
    """Test the cli method."""
    with pytest.raises(SystemExit):
        cli(["--help"])

    with pytest.raises(SystemExit):
        cli([str(data_dir), "--engine", "foo"])

    data_files = sorted(data_dir.rglob("*.nc"))
    with TemporaryDirectory() as temp_dir:
        cli([str(data_dir), "--output", temp_dir])
        new_files = sorted(Path(temp_dir).rglob("*.nc"))
    assert len(data_files) == len(new_files)
