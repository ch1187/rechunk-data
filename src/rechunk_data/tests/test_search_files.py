"""Unit tests for searching for files."""
from pathlib import Path

from rechunk_data._rechunk import _search_for_nc_files


def test_search_directory(data_dir: Path) -> None:
    """Search a directory with valid netcdf files."""
    data_files = sorted(_search_for_nc_files(data_dir))
    assert len(data_files) == 9
    search_dir_with_wild_cards = data_dir / "*"
    data_files = sorted(_search_for_nc_files(search_dir_with_wild_cards))
    assert len(data_files) == 9
    search_dir_with_wild_cards = data_dir / "*.txt"
    data_files = sorted(_search_for_nc_files(search_dir_with_wild_cards))
    assert len(data_files) == 0


def test_search_directory_with_wrong_files_type(wrong_file_type: Path) -> None:
    """Search a directory contaning files not matching the suffixes."""
    data_files = sorted(_search_for_nc_files(wrong_file_type))
    assert len(data_files) == 0
    search_dir_with_wild_cards = wrong_file_type / "*"
    data_files = sorted(_search_for_nc_files(search_dir_with_wild_cards))
    assert len(data_files) == 0
