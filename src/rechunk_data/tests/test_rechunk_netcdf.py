"""Test the actual rechunk method."""
import logging
from pathlib import Path
import time
from tempfile import NamedTemporaryFile, TemporaryDirectory

import dask
import pytest
from rechunk_data import rechunk_netcdf_file, rechunk_dataset
from rechunk_data._rechunk import _save_dataset


def test_rechunk_data_dir_with_overwrite(data_dir: Path) -> None:
    """The the overwriting of files in a directory."""
    data_files = {f.name: f.stat().st_atime for f in data_dir.rglob(".nc")}
    with dask.config.set({"array.chunk-size": "2kiB"}):
        rechunk_netcdf_file(data_dir)
    new_files = {f.name: f.stat().st_atime for f in data_dir.rglob(".nc")}
    assert len(data_files) == len(new_files)
    assert list(data_files.keys()) == list(new_files.keys())


def test_rechunk_data_dir_without_overwrite(data_dir: Path) -> None:
    """Testing the creation of new datafiles from a folder."""
    with TemporaryDirectory() as temp_dir:
        rechunk_netcdf_file(data_dir, Path(temp_dir))
        new_files = sorted(f.relative_to(temp_dir) for f in Path(temp_dir).rglob(".nc"))
    old_files = sorted(f.relative_to(data_dir) for f in data_dir.rglob(".nc"))
    assert new_files == old_files


def test_rechunk_single_data_file(data_file: Path) -> None:
    """Testing rechunking of single data files."""

    a_time = float(data_file.stat().st_mtime)
    time.sleep(0.5)
    with dask.config.set({"array.chunk-size": "1MiB"}):
        rechunk_netcdf_file(data_file)
        assert a_time < float(data_file.stat().st_mtime)
        with NamedTemporaryFile(suffix=".nc") as temp_file:
            rechunk_netcdf_file(data_file, Path(temp_file.name))
            assert Path(temp_file.name).exists()


def test_rechunk_dataset(small_chunk_data) -> None:
    """Test rechunking an xarray dataset."""
    with dask.config.set({"array.chunk-size": "1MiB"}):
        new_data = rechunk_dataset(small_chunk_data)
        assert list(new_data.data_vars) == list(small_chunk_data.data_vars)


def test_wrong_or_format(small_chunk_data, caplog) -> None:
    """Testing wrong file format."""
    caplog.clear()
    caplog.set_level(logging.DEBUG)
    with NamedTemporaryFile(suffix=".nc") as temp:
        temp_file = Path(temp.name)
        rechunk_netcdf_file(temp_file)
        _, loglevel, message = caplog.record_tuples[-1]
        assert loglevel == logging.ERROR
        assert "Error while" in message
        _save_dataset(small_chunk_data, temp_file, {}, "foo")
        _, loglevel, message = caplog.record_tuples[-1]
        _save_dataset(small_chunk_data, temp_file, {"foo": "bar"}, "foo")
        _, loglevel, message = caplog.record_tuples[-1]
        assert loglevel == logging.ERROR


def test_wrong_engine(small_chunk_data) -> None:
    with pytest.raises(ValueError):
        rechunk_dataset(small_chunk_data, engine="foo")
