"""Unit tests for rechunking the data."""
import dask
import xarray as xr

from rechunk_data._rechunk import _rechunk_dataset


def test_rechunking_small_data(
    small_chunk_data: xr.Dataset, variable_name: str
) -> None:
    """Rechunking of small datasets should have no effect."""
    chunks = small_chunk_data[variable_name].data.chunksize
    dset, encoding = _rechunk_dataset(small_chunk_data, "h5netcdf")
    assert dset[variable_name].data.chunksize == chunks
    dset, encoding = _rechunk_dataset(small_chunk_data.load(), "h5netcdf")
    assert encoding == {}


def test_empty_dataset(empty_data):
    """Test handling of empyt data."""
    with dask.config.set({"array.chunk-size": "0.01b"}):
        dset, encoding = _rechunk_dataset(empty_data, "h5netcdf")
    assert encoding == {}


def test_rechunking_large_data(
    large_chunk_data: xr.Dataset, variable_name: str
) -> None:
    """Check the rechunk_data of a large dataset."""
    chunks = (
        large_chunk_data[variable_name]
        .data.rechunk({0: "auto", 1: "auto", 2: None, 3: None})
        .chunksize
    )
    dset, encoding = _rechunk_dataset(large_chunk_data, "h5netcdf")
    assert encoding[variable_name]["chunksizes"] == chunks
    assert dset[variable_name].data.chunksize == chunks
