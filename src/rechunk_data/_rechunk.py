"""Rechunking module."""

import os
import logging
from pathlib import Path
from typing import cast, Any, Dict, Hashable, Generator, Optional, Tuple
from typing_extensions import Literal

from dask.utils import format_bytes
from dask.array.core import Array
import xarray as xr


logging.basicConfig(
    format="%(name)s - %(levelname)s - %(message)s", level=logging.ERROR
)
logger = logging.getLogger("rechunk-data")

ENCODINGS = dict(
    h5netcdf={
        "_FillValue",
        "complevel",
        "chunksizes",
        "dtype",
        "zlib",
        "compression_opts",
        "shuffle",
        "fletcher32",
        "compression",
        "contiguous",
    },
    netcdf4={
        "contiguous",
        "complevel",
        "zlib",
        "shuffle",
        "_FillValue",
        "least_significant_digit",
        "chunksizes",
        "fletcher32",
        "dtype",
    },
)


def _search_for_nc_files(input_path: Path) -> Generator[Path, None, None]:
    suffixes = [".nc", "nc4"]
    input_path = input_path.expanduser().absolute()
    if input_path.is_dir() and input_path.exists():
        nc_iter = input_path.rglob("*.*")
    elif input_path.is_file() and input_path.exists():
        nc_iter = cast(Generator[Path, None, None], iter([input_path]))
    else:
        # This could be a path with a glob pattern, let's try to construct it
        nc_iter = input_path.parent.rglob(input_path.name)
    for ncfile in nc_iter:
        if ncfile.suffix in suffixes:
            yield ncfile


def _save_dataset(
    dset: xr.Dataset,
    file_name: Path,
    encoding: Dict[Hashable, Dict[str, Any]],
    engine: Literal["netcdf4", "h5netcdf"],
    override: bool = False,
) -> None:
    if not encoding and not override:
        logger.debug("Chunk size already optimized for %s", file_name.name)
        return
    logger.debug("Saving file to %s using %s engine", str(file_name), engine)
    try:
        dset.to_netcdf(
            file_name,
            engine=engine,
            encoding=encoding,
        )
    except Exception as error:
        logger.error("Saving to file failed: %s", str(error))


def _rechunk_dataset(
    dset: xr.Dataset,
    engine: Literal["h5netcdf", "netcdf4"],
) -> Tuple[xr.Dataset, Dict[Hashable, Dict[str, Any]]]:
    encoding: Dict[Hashable, Dict[str, Any]] = {}
    try:
        _keywords = ENCODINGS[engine]
    except KeyError as error:
        raise ValueError(
            "Only the following engines are supported: ', '.join(ENCODINGS.keys())"
        ) from error
    for data_var in dset.data_vars:
        var = str(data_var)
        if (
            not isinstance(dset[var].data, Array)
            or "bnds" in var
            or "rotated_pole" in var
        ):
            logger.debug("Skipping rechunking variable %s", var)
            continue
        logger.debug("Rechunking variable %s", var)
        chunks: Dict[int, Optional[str]] = {}
        for i, dim in enumerate(map(str, dset[var].dims)):
            if "lon" in dim.lower() or "lat" in dim.lower() or "bnds" in dim.lower():
                chunks[i] = None
            else:
                chunks[i] = "auto"
        old_chunks = dset[var].encoding.get("chunksizes")
        new_chunks = dset[var].data.rechunk(chunks).chunksize
        if new_chunks == old_chunks:
            logger.debug("%s: chunk sizes already optimized, skipping", var)
            continue
        dset[var] = dset[var].chunk(dict(zip(dset[var].dims, new_chunks)))
        logger.debug(
            "%s: old chunk size: %s, new chunk size: %s",
            var,
            old_chunks,
            new_chunks,
        )
        logger.debug("Settings encoding of variable %s", var)
        encoding[data_var] = {
            str(k): v for k, v in dset[var].encoding.items() if str(k) in _keywords
        }
        if engine != "netcdf4" or encoding[data_var].get("contiguous", False) is False:
            encoding[data_var]["chunksizes"] = new_chunks
    return dset, encoding


def rechunk_dataset(
    dset: xr.Dataset, engine: Literal["h5netcdf", "netcdf4"] = "netcdf4"
) -> xr.Dataset:
    """Rechunk a xarray dataset.

    Parameters
    ----------
    dset: xarray.Dataset
        Input dataset that is going to be rechunked
    engine: str, default: netcdf4
        The netcdf engine used to create the new netcdf file.

    Returns
    -------
    xarray.Dataset: rechunked dataset
    """
    data, _ = _rechunk_dataset(dset.chunk(), engine)
    return data


def rechunk_netcdf_file(
    input_path: os.PathLike,
    output_path: Optional[os.PathLike] = None,
    decode_cf: bool = True,
    engine: Literal["h5netcdf", "netcdf4"] = "netcdf4",
) -> None:
    """Rechunk netcdf files.

    Parameters
    ----------
    input_path: os.PathLike
        Input file/directory. If a directory is given all ``.nc`` in all sub
        directories will be processed
    output_path: os.PathLike
        Output file/directory of the chunked netcdf file(s). Note: If ``input``
        is a directory output should be a directory. If None given (default)
        the ``input`` is overidden.
    decode_cf: bool, default: True
        Whether to decode these variables, assuming they were saved according
        to CF conventions.
    engine: str, default: netcdf4
        The netcdf engine used to create the new netcdf file.
    """
    input_path = Path(input_path).expanduser().absolute()
    for input_file in _search_for_nc_files(input_path):
        logger.info("Working on file: %s", str(input_file))
        if output_path is None:
            output_file = input_file
        elif Path(output_path).expanduser().absolute().is_dir():
            output_file = Path(output_path).expanduser().absolute()
            output_file /= input_file.relative_to(input_path)
        else:
            output_file = Path(output_path)
        output_file.parent.mkdir(exist_ok=True, parents=True)
        try:
            with xr.open_mfdataset(
                str(input_file),
                parallel=True,
                decode_cf=decode_cf,
            ) as nc_data:
                new_data, encoding = _rechunk_dataset(nc_data, engine)
                if encoding:
                    logger.debug(
                        "Loading data into memory (%s).",
                        format_bytes(new_data.nbytes),
                    )
                    new_data = new_data.load()
        except Exception as error:
            logger.error(
                "Error while processing file %s: %s",
                str(input_file),
                str(error),
            )
            continue
        _save_dataset(
            new_data,
            output_file.with_suffix(input_file.suffix),
            encoding,
            engine,
            override=output_file != input_file,
        )
