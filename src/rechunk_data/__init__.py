"""CLI for rechunking netcdf data."""

import argparse
import logging
from pathlib import Path
from typing import List, Optional
from ._rechunk import (
    rechunk_netcdf_file,
    rechunk_dataset,
    logger,
)

__version__ = "2310.0.1"
PROGRAM_NAME = "rechunk-data"


def parse_args(argv: Optional[List[str]]) -> argparse.Namespace:
    """Parse arguments for the command line parser."""

    parser = argparse.ArgumentParser(
        prog=PROGRAM_NAME,
        description=(
            "Rechunk input netcdf data to optimal chunk-size."
            " approx. 126 MB per chunk"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "input",
        type=Path,
        help=(
            "Input file/directory. If a directory is given "
            "all ``.nc`` files in all sub directories will be "
            "processed"
        ),
    )
    parser.add_argument(
        "--output",
        type=Path,
        help=(
            "Output file/directory of the chunked netcdf "
            "file(s). Note: If ``input`` is a directory output should be a"
            " directory. If None given (default) the ``input`` is overidden."
        ),
        default=None,
    )
    parser.add_argument(
        "--netcdf-engine",
        help=("The netcdf engine used to create the new netcdf file."),
        choices=("h5netcdf", "netcdf4"),
        default="netcdf4",
        type=str,
    )
    parser.add_argument(
        "--skip-cf-convention",
        help="Do not assume assume data variables follow CF conventions.",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "-v",
        action="count",
        default=0,
        help="Increase verbosity",
    )
    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version=f"%(prog)s {__version__}",
    )
    args = parser.parse_args(argv)
    logger.setLevel(max(logging.ERROR - (10 + args.v * 10), 10))
    return args


def cli(argv: Optional[List[str]] = None) -> None:
    """Command line interface calling the rechunking method."""
    args = parse_args(argv)
    rechunk_netcdf_file(
        args.input,
        args.output,
        engine=args.netcdf_engine,
        decode_cf=args.skip_cf_convention is False,
    )
