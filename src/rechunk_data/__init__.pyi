import argparse
import os
from pathlib import Path
from typing import (
    Any,
    Generator,
    Mapping,
    Dict,
    Hashable,
    List,
    Optional,
    Tuple,
)
from typing_extensions import Literal

import xarray as xr

def parse_args() -> argparse.Namespace: ...
def _search_for_nc_files(input_path: Path) -> Generator[Path, None, None]: ...
def _save_dataset(
    dset: xr.Dataset,
    file_name: Path,
    encoding: Dict[Hashable, Dict[str, Any]],
    engine: Literal["netcdf4", "h5netcdf"],
) -> None: ...
def _rechunk_dataset(
    dset: xr.Dataset, engine: Literal["netcdf4", "h5netcdf"]
) -> Tuple[xr.Dataset, Dict[Hashable, Dict[str, Any]]]: ...
def rechunk_dataset(
    dset: xr.Dataset, engine: Literal["netcdf4", "h5netcdf"] = ...
) -> xr.Dataset: ...
def rechunk_netcdf_file(
    input_path: os.PathLike,
    output_path: Optional[os.PathLike] = ...,
    engine: Literal["netcdf4", "h5netcdf"] = ...,
) -> None: ...
def cli(
    argv: Optional[List[str]] = ...,
) -> None: ...
